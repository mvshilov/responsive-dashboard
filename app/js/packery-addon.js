$(function() {
  Packery.prototype.getWidgetsWithPositions = function () {
    var _this = this;
    return this.items.map(function (item) {
      return {
        id: item.element.getAttribute("widget-id"),
        pos: item.rect.x / _this.packer.width
      }
    });
  };

  Packery.prototype.initShiftLayout = function () {
    var _this = this;

    var posNotSet = false
    _this.items.map(function (item) {
      var pos = item.element.getAttribute("widget-pos")

      if ((pos==="" || isNaN(pos)) && !posNotSet){
        posNotSet = true;
      }
    });

    if (posNotSet) {
      _this.layout();
      return;
    }

    this._resetLayout();
    this.items.map(function (item) {
      var pos = item.element.getAttribute("widget-pos");
      item.rect.x = pos * _this.packer.width;
    });

    _this.shiftLayout();
  };
});