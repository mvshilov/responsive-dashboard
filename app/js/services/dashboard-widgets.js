app.service("widgetsFactory", function widgetsFactory(){

  var widgets = {
    "1x1": {
      name: "1x1",
      width: 1,
      height: 1,
    },
    "1x2": {
      name: "1x2",
      width: 1,
      height: 2,
    },
    "1x3": {
      name: "1x3",
      width: 1,
      height: 3,
    }
    ,
    "1x4": {
      name: "1x4",
      width: 1,
      height: 3,
    },
    "2x1": {
      name: "2x1",
      width: 2,
      height: 1,
    },
    "2x2": {
      name: "2x2",
      width: 2,
      height: 2,
    },
    "2x3": {
      name: "2x3",
      width: 2,
      height: 3,
    },
    "2x4": {
      name: "2x4",
      width: 2,
      height: 4,
    },
    "3x1": {
      name: "3x1",
      width: 3,
      height: 1,
    },
    "3x2": {
      name: "3x2",
      width: 3,
      height: 2,
    },
    "3x3": {
      name: "3x3",
      width: 3,
      height: 3,
    },
    "3x4": {
      name: "3x4",
      width: 3,
      height: 4,
    },
    "4x1": {
      name: "4x1",
      width: 4,
      height: 1,
    },
    "4x2": {
      name: "4x2",
      width: 4,
      height: 2,
    },
    "4x3": {
      name: "4x4",
      width: 4,
      height: 3,
    },
    "4x4": {
      name: "4x4",
      width: 4,
      height: 4,
    }
  };

  return {
    getWidget: function(id) {
      return widgets[id];
    },
    getAllWidgets: function(){
      return widgets;
    }
  }
});