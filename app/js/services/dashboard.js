app.service("dashboardFactory", function widgetsFactory(){
  var defaultWidgets = [
    {
      id: "1x1"
    },
    {
      id: "1x1"
    },
    {
      id: "2x1"
    },
    {
      id: "2x1"
    },
    {
      id: "1x1"
    },
    {
      id: "1x1"
    },
    {
      id: "4x1"
    },
    {
      id: "4x1"
    },
    {
      id: "2x2"
    },
    {
      id: "2x2"
    },
    {
      id: "4x4"
    },
    {
      id: "2x2"
    },
    {
      id: "2x2"
    }
  ];
  var localStorageKey = "widgets";

  return {
    getDefaultWidgets: function(){
      return angular.copy(defaultWidgets);
    },
    getWidgets: function() {
      var result = angular.fromJson(localStorage.localStorageKey);

      if (!angular.isArray(result)) {
        result = defaultWidgets;
      }

      return result;
    },
    setWidgets: function(wiggets){
      localStorage.localStorageKey = angular.toJson(wiggets);
    },
  }
});