app.controller('selectWidgetModalController', function ($scope, $modalInstance, widgetsFactory, items) {

  $scope.items = widgetsFactory.getAllWidgets();
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
