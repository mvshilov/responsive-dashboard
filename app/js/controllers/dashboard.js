app.controller('dashboardController', function ($timeout, dashboardFactory, widgetsFactory, $modal, $log) {
  var _this = this;

  this.widgets = dashboardFactory.getWidgets();
  this.getWidget = widgetsFactory.getWidget;

  var packeryOptions = {
    itemSelector: '.widget-container',
    columnWidth: '.widget-container',
    percentPosition: true,
    initLayout: false,
    transitionDuration: '0.3s',
    shiftPercentResize: true
  }

  var $grid;

  var initPackery = function(){
    $grid = angular.element('.dashboard').packery(packeryOptions);

    // init layout with saved positions
    $grid.packery('initShiftLayout');

    // make all grid-items draggable
    $grid.find('.widget-container').each(function (i, gridItem) {
      var draggie = new Draggabilly(gridItem);
      $grid.packery('bindDraggabillyEvents', draggie);
    });
  };

  $timeout(function() {
    initPackery()

    // save drag positions on event
    $grid.on( 'dragItemPositioned', function() {
      // save drag positions
      var widgets = $grid.packery('getWidgetsWithPositions');
      dashboardFactory.setWidgets(widgets);
    });

    // remove widget
    _this.removeWidget = function($event){
      $grid.packery('remove', angular.element($event.target).parent());
      dashboardFactory.setWidgets($grid.packery('getWidgetsWithPositions'));
    }

    // restore defaults
    _this.restoreDefaults = function() {
      _this.widgets = [];
      $timeout(function() {
        $grid.packery("destroy");
        _this.widgets = dashboardFactory.getDefaultWidgets();
        $timeout(function() {
          initPackery();
          dashboardFactory.setWidgets($grid.packery('getWidgetsWithPositions'))
        });
      })
    }

    // restore defaults
    _this.removeAll = function() {
      _this.widgets = [];
      $timeout(function() {
        $grid.packery("destroy");
        $timeout(function() {
          initPackery();
          dashboardFactory.setWidgets($grid.packery('getWidgetsWithPositions'))
        });
      })
    }

    // add widget
    _this.addWidget = function() {
      var modalInstance = $modal.open({
        //animation: $scope.animationsEnabled,
        templateUrl: 'myModalContent.html',
        controller: 'selectWidgetModalController',
        //size: size,
        resolve: {
          items: function () {
            return [];
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        // destroy packery
        $grid.packery("destroy");

        // add new widget into collection
        _this.widgets[_this.widgets.length] = {
          id: selectedItem.name,
        };

        // re-init packery
        $timeout(function() {
          initPackery();
          // and save the results
          dashboardFactory.setWidgets($grid.packery('getWidgetsWithPositions'))
        })

      }, function () {

      });
    }
  });
});