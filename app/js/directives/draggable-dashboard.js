app.directive("draggableDashboard", function($log, widgetsFactory, $timeout) {

  return {

    restrict: "E",
    templateUrl: "views/draggable-dashboard.html",
    scope: {
      elements: "=",
      onDragAndDropPositioned: "="
    },
    controller: function($scope) {
      $scope.getWidget = widgetsFactory.getWidget;
    },
    link: function($scope, element) {
      $timeout(function(){
        var $grid = angular.element('.dashboard').packery({
          itemSelector: '.widget-container',
          columnWidth: '.widget-container',
          percentPosition: true,
          initLayout: false,
          //transitionDuration: '0.3s',
          shiftPercentResize: true
        });

        // init layout with saved positions
        $grid.packery('initShiftLayout');

        $scope.widgets = $grid.packery('getWidgetsWithPositions');

        // make all grid-items draggable
        $grid.find('.widget-container').each(function (i, gridItem) {
          var draggie = new Draggabilly(gridItem);
          // bind drag events to Packery
          $grid.packery('bindDraggabillyEvents', draggie);
        });

        // save drag positions on event
        //$grid.on( 'dragItemPositioned', $scope.onDragAndDropPositioned);
        $grid.on( 'dragItemPositioned', function(){
          $scope.elements = $grid.packery('getWidgetsWithPositions');
          $scope.$apply();
          $timeout(function() {
            $scope.onDragAndDropPositioned()
          })
        });
      });
    }
  };
});