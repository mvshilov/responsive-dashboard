var gulp = require('gulp');
var livereload = require('gulp-livereload');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var sequence = require('run-sequence');

gulp.task('connect', function () {
    connect.server({
        root: ['app', 'bower_components'],
        port: 8000,
        livereload: true
    });
});

gulp.task('watch', ['connect'], function () {
    gulp.watch(['./app/**/*.html', './app/**/*.js'], ['livereload']);
    gulp.watch(['./sass/**/*.scss'], ['livereload:sass']);
});

gulp.task('sass', function() {
    return gulp.src('./sass/app.scss')
      .pipe(sass())
      .pipe(gulp.dest('./app/css'));
});

gulp.task('livereload', function() {
    return gulp.src(['app/*.html'])
      .pipe(connect.reload());
});

gulp.task('livereload:sass', function() {
    sequence('sass', 'livereload');
});
